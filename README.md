# What is this?

- Rustaceans (all of them named Ferris) are on their day off, just looking around for kelp to eat.
- You are a bug. So the Rust mascots will eat you all the same as the kelp that spawns.
- You can try to eat the kelps that spawn while avoiding all the Rustaceans.

# Special install instructions

This was made with SDL2 which is cross platform, but has only been tested on MacOS so far.

- Have Homebrew installed.
- `brew install sdl2 sdl2_image sdl2_ttf`
- Check out this repository and simply do `cargo run`

# Controls 

- Move: WASD or Arrow Keys
- Reset: R
- Exit: Escape
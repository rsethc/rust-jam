use rand::{thread_rng, Rng};
use sdl2::{
    event::Event,
    image::LoadTexture,
    keyboard::Keycode,
    messagebox::MessageBoxFlag,
    pixels::{self, Color},
    rect::Rect,
    render::Canvas,
    ttf::Font,
    video::Window,
    TimerSubsystem,
};

/*
    TO DO: License compliance

    Bug asset: https://commons.wikimedia.org/wiki/File:Bembidion_decoratum_(Duftschmid,_1812).png

    Rustaceans assets: https://rustacean.net/

    Kelp: AI generated (Microsoft Bing chat app)
*/

fn title_sequence() -> Result<(), String> {
    Ok(())
}

struct Rustacean {
    tex_id: usize,
    x: f64,
    y: f64,
    dir: Dir,
}
struct Rustaceans {
    crabs: Vec<Rustacean>,
}
enum Dir {
    Left,
    Right,
    Up,
    Down,
}
const CRABS_WIDTH: u32 = 100;
fn rndx() -> f64 {
    thread_rng().gen_range(0..1920 - CRABS_WIDTH) as f64
}
fn rndy() -> f64 {
    thread_rng().gen_range(0..1080 - CRABS_WIDTH / 2) as f64
}
const FAR_LEFT: f64 = -(CRABS_WIDTH as f64);
const FAR_RIGHT: f64 = 1920.0;
const FAR_UP: f64 = -(CRABS_WIDTH as f64) / 1.5;
const FAR_DOWN: f64 = 1080.0;
impl Rustaceans {
    fn new() -> Rustaceans {
        Self { crabs: Vec::new() }
    }
    fn spawn(&mut self, tex_id: usize) {
        let crab_id = self.crabs.len();
        self.crabs.push(Rustacean {
            tex_id,
            x: 0.0,
            y: 0.0,
            dir: Dir::Right,
        });
        self.reposition(crab_id);
        let crab = &mut self.crabs[crab_id];
        match crab.dir {
            Dir::Left | Dir::Right => {
                crab.x = rndx();
            }
            Dir::Up | Dir::Down => {
                crab.y = rndy();
            }
        }
    }
    fn reposition(&mut self, crab_id: usize) {
        let crab = &mut self.crabs[crab_id];
        crab.dir = match thread_rng().gen_range(0..4) {
            0 => {
                crab.x = FAR_RIGHT;
                crab.y = rndy();
                Dir::Left
            }
            1 => {
                crab.x = FAR_LEFT;
                crab.y = rndy();
                Dir::Right
            }
            2 => {
                crab.x = rndx();
                crab.y = FAR_DOWN;
                Dir::Up
            }
            _ => {
                crab.x = rndx();
                crab.y = FAR_UP;
                Dir::Down
            }
        };
    }
    fn step(&mut self, delta: f64) {
        const CRAB_VEL: f64 = 300.0;
        let move_dist = delta * CRAB_VEL;
        for crab_id in 0..self.crabs.len() {
            let mut crab = &mut self.crabs[crab_id];
            match crab.dir {
                Dir::Left => {
                    crab.x -= move_dist;
                    if crab.x < FAR_LEFT {
                        self.reposition(crab_id);
                    }
                }
                Dir::Right => {
                    crab.x += move_dist;
                    if crab.x >= FAR_RIGHT {
                        self.reposition(crab_id);
                    }
                }
                Dir::Up => {
                    crab.y -= move_dist;
                    if crab.y <= FAR_UP {
                        self.reposition(crab_id);
                    }
                }
                Dir::Down => {
                    crab.y += move_dist;
                    if crab.y >= FAR_DOWN {
                        self.reposition(crab_id);
                    }
                }
            }
        }
    }
}

fn crab_rect(crab: &Rustacean, crab_aspects: &Vec<f64>) -> Rect {
    let rect = Rect::new(
        crab.x as i32,
        crab.y as i32,
        CRABS_WIDTH,
        (CRABS_WIDTH as f64 / crab_aspects[crab.tex_id]) as u32,
    );
    rect
}

struct Kelp {
    tex_id: usize,
    rect: Rect,
}

fn game_main(
    mut canvas: Canvas<Window>,
    mut input: sdl2::EventPump,
    timing: TimerSubsystem,
    font: Font,
) -> Result<(), String> {
    canvas
        .set_logical_size(1920, 1080)
        .map_err(|err| err.to_string())?;

    title_sequence()?;

    let loader = canvas.texture_creator();
    let bug_tex = loader.load_texture("assets/bug.png")?;
    let bug_aspect = bug_tex.query().width as f64 / bug_tex.query().height as f64;

    let mut prior_time = timing.performance_counter();

    'main: loop {
        let mut kelp_spawn_accumulate = 0.0;
        const KELP_SPAWN_INTERVAL: f64 = 1.0;

        let mut crabs = Rustaceans::new();

        let crab_paths = vec![
            "cuddlyferris.png",
            "rustacean-flat-gesture.png",
            "rustacean-flat-happy.png",
            "rustacean-flat-noshadow.png",
            "rustacean-orig-noshadow.png",
            "rustacean-unsafe.png",
        ];
        let mut crab_textures = Vec::new();
        let mut crab_aspects = vec![];
        for path in crab_paths {
            let tex = loader.load_texture("assets/".to_string() + path)?;
            let info = tex.query();
            let aspect = info.width as f64 / info.height as f64;
            crab_textures.push(tex);
            crab_aspects.push(aspect);
        }
        let mut kelp_textures = Vec::new();
        for i in 1..=4 {
            let tex = loader.load_texture(format!("assets/kelp{}.png", i))?;
            kelp_textures.push(tex);
        }

        let difficulty = 2;
        for i in 0..crab_textures.len() * difficulty {
            crabs.spawn(i % crab_textures.len());
        }

        let mut bug_x: f64 = 1920.0 * 0.5;
        let mut bug_y: f64 = 1080.0 * 0.5;

        const BUG_W: u32 = 90;
        let bug_h: u32 = (BUG_W as f64 / bug_aspect) as u32;

        let mut move_right = false;
        let mut move_left = false;
        let mut move_up = false;
        let mut move_down = false;

        let mut alive = true;

        let mut kelps = Vec::new();

        let mut score_value = 0;

        loop {
            while let Some(event) = input.poll_event() {
                match event {
                    Event::Quit { timestamp: _ } => {
                        break 'main;
                    }
                    Event::KeyDown { keycode, .. } => {
                        if let Some(keycode) = keycode {
                            match keycode {
                                Keycode::Escape => {
                                    break 'main;
                                }
                                Keycode::W | Keycode::Up => {
                                    move_up = true;
                                }
                                Keycode::S | Keycode::Down => {
                                    move_down = true;
                                }
                                Keycode::D | Keycode::Right => {
                                    move_right = true;
                                }
                                Keycode::A | Keycode::Left => {
                                    move_left = true;
                                }
                                Keycode::R => {
                                    continue 'main;
                                }
                                _ => {}
                            }
                        }
                    }
                    Event::KeyUp { keycode, .. } => {
                        if let Some(keycode) = keycode {
                            match keycode {
                                Keycode::W | Keycode::Up => {
                                    move_up = false;
                                }
                                Keycode::S | Keycode::Down => {
                                    move_down = false;
                                }
                                Keycode::D | Keycode::Right => {
                                    move_right = false;
                                }
                                Keycode::A | Keycode::Left => {
                                    move_left = false;
                                }
                                _ => {}
                            }
                        }
                    }
                    _ => {}
                }
            }

            canvas.set_draw_color(pixels::Color::RGB(0, 0, 0));
            canvas.clear();
            if alive {
                canvas.set_draw_color(pixels::Color::RGB(0x3F, 0x7F, 0xBF));
            } else {
                canvas.set_draw_color(pixels::Color::RGB(0xFF, 0, 0));
            }

            canvas.fill_rect(None)?;

            let current_time = timing.performance_counter();
            let delta = (current_time - prior_time) as f64 / timing.performance_frequency() as f64;
            prior_time = current_time;

            if alive {
                const BUG_SPEED: f64 = 600.0;
                let bug_move = delta * BUG_SPEED;
                if move_right {
                    bug_x += bug_move;
                    if bug_x > (1920 - BUG_W) as f64 {
                        bug_x = (1920 - BUG_W) as f64;
                    }
                }
                if move_left {
                    bug_x -= bug_move;
                    if bug_x < 0.0 {
                        bug_x = 0.0;
                    }
                }
                if move_up {
                    bug_y -= bug_move;
                    if bug_y < 0.0 {
                        bug_y = 0.0;
                    }
                }
                if move_down {
                    bug_y += bug_move;
                    if bug_y > (1080 - bug_h) as f64 {
                        bug_y = (1080 - bug_h) as f64;
                    }
                }
            }

            let bug_rect = Rect::new(bug_x as i32, bug_y as i32, BUG_W, bug_h);

            crabs.step(delta);

            kelp_spawn_accumulate += delta;
            if kelp_spawn_accumulate >= KELP_SPAWN_INTERVAL {
                kelp_spawn_accumulate = 0.0;
                let tex_id = thread_rng().gen_range(0..4);
                const KELP_H: u32 = 150;
                let tex_info = kelp_textures[tex_id].query();
                let aspect = tex_info.width as f64 / tex_info.height as f64;
                let kelp_w = (KELP_H as f64 * aspect) as u32;
                kelps.push(Kelp {
                    tex_id: thread_rng().gen_range(0..4),
                    rect: Rect::new(rndx() as i32, rndy() as i32, kelp_w, KELP_H),
                });
            }

            let mut kelp_id = 0;
            loop {
                if kelp_id >= kelps.len() {
                    break;
                }

                let kelp = &kelps[kelp_id];
                canvas.copy(&kelp_textures[kelp.tex_id], None, kelp.rect)?;

                let mut remove = false;

                if alive {
                    // Check against player, kelps in contact will be counted toward score.
                    if bug_rect.has_intersection(kelp.rect) {
                        score_value += 1;

                        remove = true;
                    }
                }

                // Check against the Ferris'es because they can also eat the kelps.
                for crab in &crabs.crabs {
                    let cr = crab_rect(&crab, &crab_aspects);
                    if cr.has_intersection(kelp.rect) {
                        remove = true;
                    }
                }

                if remove {
                    kelps.remove(kelp_id);
                } else {
                    kelp_id += 1;
                }
            }

            for crab in &crabs.crabs {
                let rect = crab_rect(&crab, &crab_aspects);
                canvas.copy(&crab_textures[crab.tex_id], None, rect)?;

                if rect.has_intersection(bug_rect) {
                    alive = false;
                }
            }

            canvas.copy(&bug_tex, None, bug_rect)?;

            let txtrend = font
                .render(&format!("{}", score_value))
                .blended(Color::RGBA(0, 0xFF, 0, 0xFF))
                .map_err(|err| err.to_string())?;
            let txttex = txtrend.as_texture(&loader).map_err(|err| err.to_string())?;
            let txtw = txttex.query().width;
            let txth = txttex.query().height;
            canvas.copy(
                &txttex,
                None,
                Rect::new((1920 / 2 - txtw) as i32, 20, txtw, txth),
            )?;

            canvas.present();
        }
    }

    Ok(())
}

fn setup_main() -> Result<(), String> {
    let sdl = sdl2::init()?;
    let video = sdl.video()?;
    let window = video
        .window("Day Off", 1920, 1080)
        .fullscreen_desktop()
        .build()
        .map_err(|err| err.to_string())?;
    let canvas = window
        .into_canvas()
        .present_vsync()
        .build()
        .map_err(|err| err.to_string())?;

    let ttf = sdl2::ttf::init().map_err(|err| err.to_string())?;
    let font = ttf.load_font("assets/Inconsolata_SemiCondensed-SemiBold.ttf", 120)?;

    game_main(canvas, sdl.event_pump()?, sdl.timer()?, font)?;

    Ok(())
}

fn main() {
    if let Err(why) = setup_main() {
        println!("Runtime error: {}", why);
        sdl2::messagebox::show_simple_message_box(
            MessageBoxFlag::ERROR,
            "Runtime error",
            &why,
            None,
        )
        .unwrap_or_else(|why| {
            println!("Can't show message box: {}", why);
        });
    }
}
